import numpy as np
import pandas as pd
import json
import matplotlib.pyplot as plt
import sys
import os
#os.environ["QT_API"] = "pyqt5"
#%matplotlib qt



num_sec = int(sys.argv[1])
duration = int(sys.argv[2])
#num_sec = 50
#duration = 365
#num_sec = 1
#duration = 365
#time_output = pd.read_json('OutputSimResults/output_profiles_365_days_1_sez.json')
time_output = pd.read_json('OutputSimResults/output_profiles_%d_days_%d_sez.json' % (duration, num_sec))
for i in time_output.keys():
	fig = plt.figure()
	fig.set_size_inches(18.5, 10.5)
	plt.title('Sez_Cens: %s' % i)
	plt.xlabel('Time (10-mins)')
	plt.ylabel('Power [W]')

	#plt.plot(time_output[i][1].values())#,'Net')
	plt.plot(time_output[i]['Pload'].values())#,'Load')
	plt.plot(time_output[i]['Pprod'].values())#,'Prod')
	plt.gca().legend(('Load','Prod'))

	fig.savefig('OutputSimResults/Sim_%d_days_%s_sez' % (duration, i))

#num_sec = 10
#duration = 1




attributes = pd.read_json('OutputSimResults/output_attributes_%d_days_%d_sez.json' % (duration, num_sec), orient = 'index')
values = pd.read_json('OutputSimResults/output_values_%d_days_%d_sez.json' % (duration, num_sec))

mean_val = round(values.mean(axis=1),2)
max_val = round(values.max(axis=1),2)
min_val = round(values.min(axis=1),2)
val = pd.concat([mean_val,max_val,min_val], axis = 1)
val.columns = ['MEAN_VALUE','MAX_VALUE','MIN_VALUE']


results = pd.concat([attributes,val])

row = pd.DataFrame({'MEAN_VALUE':'MEAN_VALUE','MAX_VALUE':'MAX_VALUE','MIN_VALUE':'MIN_VALUE'},index=[1])
results = pd.concat([results.loc[:'time_sim'],row,results[-6:]]).fillna('')
results.columns = ['']*len(results.columns)
as_list = results.index.to_list()
idx = as_list.index(1)
as_list[idx] = ''
results.index = as_list


results.to_csv('OutputSimResults/final_results_%d_days_%d_sez.csv' % (duration, num_sec))