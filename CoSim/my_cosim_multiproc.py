#how to call from terminal: python my_start.py num_sec time(days)

#import mosaik
#import mosaik.util
import geopandas as gpd
import os
import pandas as pd
import numpy as np
import pickle
import itertools
import time
from pathlib import Path
import json
from pvsimu import gis_pv_sim
from homesimu import Home_new as Home
import meter_cosim
import re
import collections
#path = os.path.dirname(__file__)
path = os.path.dirname('.')
import multiprocessing
from functools import partial

zero_time = time.time()



######## SETTINGS

days = int(input('Hows many days? '))
num_sec = int(input('Hows many sections? '))
if num_sec == 1:
    from_sec = int(input('Which section you want to simuate? '))
    to_sec = from_sec
else:
    from_sec = int(input('From which section you want to simulate? '))
    to_sec = int(input('To which section you want to simulate? '))


collect = input('Do you want to save power profiles? Y/N ').upper()
if collect == ('Y'):
    collect = True
else:
    collect = False





# Time interval

START_DATE = '2015-09-22 00:00:00' #YYYY/MM/dd 
start_date = pd.to_datetime(START_DATE).tz_localize('UTC')
end_date = start_date + days*pd.Timedelta('1D') - pd.Timedelta('5m')

# PVs Specifications
PVSystemsSpec = {'Tc_noct':45, 'T_ex_noct':20, 'a_p':0.0038, 'ta':0.9, 'P_module':283, 'G_noct':800, 'G_stc':1000, 'ModuleArea':1.725, 'Tc_stc':25.0}





######## IMPORT DATA

# Photovoltaic from DSM
gdf_PVs_tot = gpd.read_file('pvsimu/area_turin/areasuit.shp', crs = '4326')
gdf_PVs_tot = gdf_PVs_tot.set_index(gdf_PVs_tot['FID'])
gdf_PVs_tot.crs = 'epsg:4326'

# Census Sections from ISTAT
gdf_section_tot = gpd.read_file('sez_cens/torino_sezcens.shp', crs = '32632')
gdf_section_tot = gdf_section_tot.sort_values(by = ['SEZ2011'])
gdf_section_tot = gdf_section_tot.to_crs(epsg = '4326')

# Houses from populationsim
houses_tot = pd.read_csv('pop_sim/synthetic_households.csv')
houses_tot = houses_tot.set_index(houses_tot['household_id'])
houses_tot = houses_tot.sort_values(by = ['SEZ2011'])

# Families from populationsim
fname = 'pop_sim/persons.pkl'
with open(fname, 'rb') as f:
    families_tot = pickle.load(f) 

# Meteo from WheatherUnderground   
METEO_FILE = 'meteo_mosaik.csv'
meteo = pd.read_csv(METEO_FILE, index_col = 'Date_time', parse_dates = [0,])
meteo = meteo.reindex(pd.date_range(start=meteo.index.min(), end=meteo.index.max(), freq='5min')).interpolate(method='linear')
meteo = meteo.loc[start_date : end_date]




# Restrict to a defined number of zones 
gdf_section = gdf_section_tot.iloc[from_sec:to_sec + 1]
gdf_section = gdf_section.set_index('SEZ2011', drop=False)

# Connect PVs with selected Section
gdf_PVs = gpd.sjoin(gdf_PVs_tot, gdf_section[['SEZ2011','geometry']],op='intersects')
gdf_PVs = gdf_PVs.sort_values(by =['SEZ2011'])

# Connect houses and families with selected Section
houses = houses_tot.loc[houses_tot['SEZ2011'].isin(gdf_section['SEZ2011'])]
families  = {key: families_tot[key] for key in houses.index.astype(str)}


#Create a meter dictionary
meter_dict = collections.defaultdict(dict)
for i,r in gdf_section.iterrows():
    meter_dict['Cens_' + str(r.SEZ2011)]['PV'] = []
    meter_dict['Cens_' + str(r.SEZ2011)]['House'] = []
    for val,p in gdf_PVs[gdf_PVs['SEZ2011'] == i].iterrows():
        meter_dict['Cens_' + str(r.SEZ2011)]['PV'].append(str(val))
    for val,p in houses[houses['SEZ2011'] == i].iterrows():
        meter_dict['Cens_' + str(r.SEZ2011)]['House'].append(str(val))



print('INITIALISING SIMULATORS...')
#INITIALISE SIMULATORS
model_PV = gis_pv_sim.GISPVsimulator()
model_PV.GisArea = gpd.GeoDataFrame(gdf_PVs.to_dict())   #DEVO PROPRIO?
model_home = Home.Simulator()
model_meter = meter_cosim.MeterSimulator()


def call_multiproc(chunks):
    start_time = time.time()


    meter = dict([chunks])  # Convert back to a dict
    meter = list(meter.keys())[0]
    num_pop = 0
    area = gdf_section.loc[int(re.sub('Cens_','',meter))]['Shape_Area']

    #ADDING MODEL TO THE SIMULATORS
    model_meter.add_model(meter)

    for FID in meter_dict[meter]['PV']:
        model_PV.add_PV(AreaPV={'FID': int(FID), 'Area': model_PV.GisArea.loc[int(FID),'Area_reale']}, PVSystemSpec = {'Tc_noct':45, 'T_ex_noct':20, 'a_p':0.0038, 'ta':0.9, 'P_module':283, 'G_noct':800, 'G_stc':1000, 'ModuleArea':1.725, 'Tc_stc':25.0})
    
    for house in meter_dict[meter]['House']:
        model_home.add_model(init_val = families[house], houseid = house)
        num_pop = num_pop + sum(families[house].values())

    time_setting = time.time() - start_time


    #START SIMULATION
    data = {}
    date_sim = start_date
    step = pd.Timedelta('5m')
    while date_sim <= end_date:

        if date_sim.minute % 15 ==0:
            data['time'] = date_sim
            data['zenith'] = meteo.loc[date_sim]['zenith']
            data['T_ext'] = meteo.loc[date_sim]['T_ext']
            data['k_b'] = meteo.loc[date_sim]['k_b']
            data['k_d'] = meteo.loc[date_sim]['k_d']
            model_PV.update_map(data)

            model_meter.models[meter].Pprod = 0

            for i in model_PV.models:
                if str(i.FID) in meter_dict[meter]['PV']:
                    model_PV.step(env_data = data, meter_FIDS = i)
                    model_meter.models[meter].Pprod = i.power_dc + model_meter.models[meter].Pprod

        if date_sim.minute % 10 == 0:

            model_meter.models[meter].Pload = 0

            for i in model_home.models:
                if str(i.houseid) in meter_dict[meter]['House']:
                    model_home.step(time=date_sim, meter_home = i)
                    model_meter.models[meter].Pload = i.Consumption['Aggregate'] + model_meter.models[meter].Pload


            model_meter.models[meter].step(collect_data=collect, duration = days, num_sec = meter, time = date_sim)

        date_sim += step


    model_meter.finalize()


    cc = time.time()
    time_sim = time.time() - start_time - time_setting
    print(time_sim)
    time_tot = time.time() - zero_time



    attributes = {}
    attributes = {'name_sez' : meter, 'area': area, 'num_PVs' : int(len(gdf_PVs)), 'num_houses' : int(len(houses)), 'num_pop' : num_pop, 'time_setting' : round(time_setting), 'time_sim' : round(time_sim), 'time_tot' : round(time_tot)}

    Path("./OutputSimResults/%d_days_%s_sez" % (days, meter)).mkdir(parents=True, exist_ok=True)
    with open(f'./OutputSimResults/%d_days_%s_sez/output_attributes.json' % (days, meter), 'w') as outfile:
        json.dump(attributes, outfile)





items = list(meter_dict.items())
chunks = [items[i] for i in range (0, len(items))]


print('SIMULATING')
pool = multiprocessing.Pool(multiprocessing.cpu_count())
pool.map(call_multiproc, chunks)
pool.close()

print('total',time.time() - zero_time)
