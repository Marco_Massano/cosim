import geopandas as gpd
import os
import pandas as pd
import numpy as np
import pickle
import itertools
import time
from pathlib import Path
import json
from homesimu import Home_new as Home
import collections
path = os.path.dirname('.')
from functools import partial
import matplotlib.pyplot as plt



days = 20

START_DATE = '2015-09-22 00:00:00' #YYYY/MM/dd 
start_date = pd.to_datetime(START_DATE).tz_localize('UTC')
end_date = start_date + days*pd.Timedelta('1D') - pd.Timedelta('5m')