import pandas as pd
import sys
import numpy as np
import semi_markov as sm
import pickle 
import pathlib
path=pathlib.Path(__file__).parent.absolute()

def main():
    fam=pd.read_csv(path.joinpath('data/input/TUS/'+'descrizione_individui.csv'),dtype=str)
    fam=fam.set_index(fam.unique_id)
    tav=pd.read_csv(path.joinpath('data/input/TUS/'+'giornaliero_righe.csv'),index_col=0,dtype=str)


    infant=fam.claseta2.astype(int).isin(['01','02'])
    kids=((fam.claseta2.astype(int).isin(['03','04'])))
    stud=((fam.newcondm=='5'))
    hwife=(fam.newcondm=='4')
    work_m=((fam.newcondm=='1') &(fam.sesso=='1'))
    work_f=((fam.newcondm=='1') &(fam.sesso=='2'))
    fullwork_m=((fam.newcondm=='1') &(fam.sesso=='1') & (fam.tempar=='1'))
    fullwork_f=((fam.newcondm=='1') &(fam.sesso=='2') & (fam.tempar=='1'))
    partialwork_m=((fam.newcondm=='1') &(fam.sesso=='1') & (fam.tempar=='2'))
    partialwork_f=((fam.newcondm=='1') &(fam.sesso=='2') & (fam.tempar=='2'))
    old_m=((fam.newcondm=='7') &(fam.sesso=='1'))
    old_f=((fam.newcondm=='7') &(fam.sesso=='2'))
    no_work_m=((fam.newcondm.isin(['2','3'])) &(fam.sesso=='1'))
    no_work_f=((fam.newcondm.isin(['2','3'])) &(fam.sesso=='1'))

    week=fam.gsett=='1'
    sat=fam.gsett=='2'
    sun=fam.gsett=='3'
    data=[tav[infant*week],tav[infant*sat],tav[infant*sun],tav[kids*week],tav[kids*sat],tav[kids*sun],tav[stud*week],tav[stud*sat],tav[stud*sun],tav[hwife*week],tav[hwife*sat],tav[hwife*sun],tav[work_m*week],tav[work_m*sat],tav[work_m*sun],tav[work_f*week],tav[work_f*sat],tav[work_f*sun],tav[old_m*week],tav[old_m*sat],tav[old_m*sun],tav[old_f*week],tav[old_f*sat],tav[old_f*sun],tav[no_work_f*week],tav[no_work_f*sat],tav[no_work_f*sun],tav[no_work_m*week],tav[no_work_m*sat],tav[no_work_m*sun],tav[fullwork_f*week],tav[fullwork_f*sat],tav[fullwork_f*sun],tav[fullwork_m*week],tav[fullwork_m*sat],tav[fullwork_m*sun],tav[partialwork_f*week],tav[partialwork_f*sat],tav[partialwork_f*sun],tav[partialwork_m*week],tav[partialwork_m*sat],tav[partialwork_m*sun]]
    fname=['infant_week','infant_sat','infant_sun','kids_week','kids_sat','kids_sun','stud_week','stud_sat','stud_sun','hwife_week','hwife_sat','hwife_sun','work_m_week','work_m_sat','work_m_sun','work_f_week','work_f_sat','work_f_sun','old_m_week','old_m_sat','old_m_sun','old_f_week','old_f_sat','old_f_sun','no_work_f_week','no_work_f_sat','no_work_f_sun','no_work_m_week','no_work_m_sat','no_work_m_sun','fullwork_f_week','fullwork_f_sat','fullwork_f_sun','fullwork_m_week','fullwork_m_sat','fullwork_m_sun','partialwork_f_week','partialwork_f_sat','partialwork_f_sun','partialwork_m_week','partialwork_m_sat','partialwork_m_sun']
# matrici infant
    for i in range(len(data)):
        model=sm.NHSMM()
        model.create(data[i])
        model.save(fname[i])
        model.reset()
        print (fname[i])
        
if __name__ == "__main__":
    import sys
    main()
