import pandas as pd
import numpy as np
from math import *
import geopandas as gpd
import os
import time
import multiprocessing
import ray
path = os.path.dirname(__file__)
from functools import partial
import time

#path = os.path.dirname(',')


class PVSystem:
	"""docstring for PVmodel"""
	def __init__(self,AreaPV={'FID':0,'Area':0},PVSystemSpec={'Tc_noct':45,'T_ex_noct':20,'a_p':0.0038,'ta':0.9,'P_module':283,
				'G_noct':800,'G_stc':1000,'ModuleArea':1.725,'Tc_stc':25.0}):
		self.Tc_stc=PVSystemSpec['Tc_stc']+273.15
		self.Tc_noct=PVSystemSpec['Tc_noct']+273.15
		self.T_ex_noct=PVSystemSpec['T_ex_noct']+273.15
		self.a_p=PVSystemSpec['a_p']
		self.ta=PVSystemSpec['ta']
		self.P_module=PVSystemSpec['P_module']
		self.G_noct=PVSystemSpec['G_noct']
		self.G_stc=PVSystemSpec['G_stc']
		self.ModuleArea=PVSystemSpec['ModuleArea']
		self.n_mp_stc=self.P_module/self.ModuleArea/self.G_stc
		self.ModuleNumber=(int)(AreaPV['Area']/self.ModuleArea)
		self.P_sytem=self.ModuleNumber * self.P_module
		self.PVSurface=self.ModuleNumber * self.ModuleArea
		self.gti=0
		self.power_dc=0
		self.n_mp=0
		self.FID=AreaPV['FID']
		
	def step(self):
		self.T_ext = 0
		T_sol = self.T_ext+273.15+0.05*self.gti
		Tc = T_sol+(self.Tc_noct-self.T_ex_noct)*(self.gti/self.G_noct)*(1-(self.n_mp_stc*(1-self.a_p*self.Tc_stc)/self.ta))/(1+(self.Tc_noct-self.T_ex_noct)*(self.gti/self.G_noct)*(self.a_p*self.n_mp_stc/self.ta))
		self.n_mp = self.n_mp_stc*(1-self.a_p*(Tc-self.Tc_stc))
		self.power_dc = self.PVSurface*self.gti*self.n_mp
		#print(self.power_dc)
		

class GISPVsimulator(object):

	def __init__(self):
		self.time=0
		self.FIDS=[]
		self.models = []
		self.data = []
		self.GisArea=None
		self.beam=pd.read_csv(os.path.join(path,"beam_clear"))
		self.diff=pd.read_csv(os.path.join(path,"diff_clear"))
		self.beam=self.beam.set_index(['FID'])
		self.diff=self.diff.set_index(['FID'])
		self.env_data=0


	def add_PV(self, AreaPV={'FID':0,'Area':0},PVSystemSpec={'Tc_noct':45,'T_ex_noct':20,'a_p':0.0038,'ta':0.9,'P_module':283,'G_noct':800,'G_stc':1000,'ModuleArea':1.725,'Tc_stc':25.0}):		
		"""Create an instances of ``Model`` with *init_val*."""
		model = PVSystem(AreaPV,PVSystemSpec)
		self.models.append(model)
		self.FIDS.append(model.FID)
		self.data.append([])  # Add list for simulation data

	def drop_not_used(self):
		self.GisArea=self.GisArea.loc[self.FIDS]
		self.beam=self.beam.loc[self.FIDS]
		self.diff=self.diff.loc[self.FIDS]



	def call_day(tuple_val,radiation,T_ext):
		i,model = tuple_val
		try:
			gti = radiation.loc[model.FID].mean()
		except: 
			gti = radiation.loc[model.FID]
		#print(gti)
		model.gti = gti
		model.T_ext = T_ext
		model.step()
		return model
		#print(model.power_dc, model.FID)

	def call_night(tuple_val,T_ext):
		i,model = tuple_val
		model.gti = 0
		model.T_ext = T_ext
		model.step()


	def step(self, env_data=None):
	######### env_data arriva dal meteo con un Pandas che ha zenith k_b k_d time 
	########### arrivano k_b e k_d dal meteo sim che fa dec.py
		month = {1:'17',2:'47',3:'75',4:'105',5:'135',6:'162',7:'198',8:'228',9:'258',10:'288',11:'318',12:'344'}
		hour = {4:'04',5:'05',6:'06',7:'07',8:'08',9:'09',10:'10',11:'11',12:'12',13:'13',14:'14',15:'15',16:'16',17:'17',18:'18',19:'19',20:'20',21:'21',22:'22'}
		minute = {0:'00',15:'25',30:'00',45:'75'}
		self.env_data = env_data
		#print(env_data)
		if self.env_data['zenith'] < 85:
			index = self.env_data['time']
			beam_calc = self.beam[month[index.month]+'_'+hour[index.hour]+'_'+minute[index.minute]]*self.env_data['k_b']
			diff_calc = self.diff[month[index.month]+'_'+hour[index.hour]+'_'+minute[index.minute]]*self.env_data['k_d']
			radiation = beam_calc+diff_calc
			radiation = radiation.fillna(0)

			p = multiprocessing.Pool()
			tuple_val = enumerate(self.models)
			part = partial(GISPVsimulator.call_day,radiation=radiation,T_ext=self.env_data['T_ext'])
			self.models = p.map(part,tuple_val)
			#print(a)
			 #, [radiation , self.env_data['T_ext']])
			p.close()
			p.join()
			#p.starmap(GISPVsimulator.call_step,[[model,radiation.loc[model.FID].mean(),self.env_data['T_ext']] for i,model in enumerate(self.models)])

		else:
			p = multiprocessing.Pool()
			tuple_val = enumerate(self.models)
			part = partial(GISPVsimulator.call_night,T_ext=self.env_data['T_ext'])
			p.map(part,tuple_val)
			p.close()
			p.join()
'''
if __name__ == '__main__':
	gdf = gpd.read_file('area_turin/areasuit.shp',crs='4326')
	gdf = gdf.set_index(gdf['FID'])
	model = GISPVsimulator()
	model.GisArea = gdf
	model.add_PV(AreaPV = {'FID':0,'Area':0},PVSystemSpec={'Tc_noct':45,'T_ex_noct':20,'a_p':0.0038,'ta':0.9,'P_module':283,
				'G_noct':800,'G_stc':1000,'ModuleArea':1.725,'Tc_stc':25.0})

'''
if __name__ == '__main__':
	start_time = time.time()

	gdf = gpd.read_file('area_turin/areasuit.shp',crs='4326')
	gdf = gdf.set_index(gdf['FID'])
	model = GISPVsimulator()
	model.GisArea = gdf
	for i in gdf.index[:10]:
		model.add_PV(AreaPV = {'FID':i,'Area':10},PVSystemSpec={'Tc_noct':45,'T_ex_noct':20,'a_p':0.0038,'ta':0.9,'P_module':283,
				'G_noct':800,'G_stc':1000,'ModuleArea':1.725,'Tc_stc':25.0})

	meteo = pd.read_csv('meteo_mosaik.csv',index_col = 'Date_time', parse_dates = [0,])
	data = {}
	print('inizio_sim')
	
	for i in range(200):
		data['time'] = meteo.index[i]
		data['zenith'] = meteo.iloc[i]['zenith']
		data['T_ext'] = meteo.iloc[i]['T_ext']
		data['k_b'] = meteo.iloc[i]['k_b']
		data['k_d'] = meteo.iloc[i]['k_d']

		
		model.step(data)
	print(time.time() - start_time)









'''

		def step(self, env_data=None):
	######### env_data arriva dal meteo con un Pandas che ha zenith k_b k_d time 
	########### arrivano k_b e k_d dal meteo sim che fa dec.py
		month = {1:'17',2:'47',3:'75',4:'105',5:'135',6:'162',7:'198',8:'228',9:'258',10:'288',11:'318',12:'344'}
		hour = {4:'04',5:'05',6:'06',7:'07',8:'08',9:'09',10:'10',11:'11',12:'12',13:'13',14:'14',15:'15',16:'16',17:'17',18:'18',19:'19',20:'20',21:'21',22:'22'}
		minute = {0:'00',15:'25',30:'00',45:'75'}
		self.env_data = env_data
		if self.env_data['zenith'] < 85:
			index = self.env_data['time']
			beam_calc = self.beam[month[index.month]+'_'+hour[index.hour]+'_'+minute[index.minute]]*self.env_data['k_b']
			diff_calc = self.diff[month[index.month]+'_'+hour[index.hour]+'_'+minute[index.minute]]*self.env_data['k_d']
			radiation = beam_calc+diff_calc
			
			radiation = radiation.fillna(0)

			for i, model in enumerate(self.models):
				try:
					gti = radiation.loc[model.FID].mean()
				except: 
					gti = radiation.loc[model.FID]


				model.gti = gti
				model.T_ext = self.env_data['T_ext']
				model.step()
				#print(model.gti)
		else:
			for i, model in enumerate(self.models):
				model.gti = 0
				model.T_ext = self.env_data['T_ext']
				model.step()
				'''