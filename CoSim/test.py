import home_dasolo_new as h
from matplotlib import pyplot as plt
import pandas as pd
#%matplotlib qt

pep={'hwife': 0,'inf': 0,'kid': 1,'no_work_f': 0,'no_work_m': 0,'old_f': 0,'old_m': 0,'stud': 1,'work_f': 1,'work_m':1,'full_m':0,'full_f':0,'partial_m':0,'partial_f':0}
home = h.Home.Home(pep)
while 'Hoven' not in home.appliancesList:
    home = h.Home.Home(pep)
home.appliancesList


P={}
for name in home.Consumption.keys():
    P[name]=[]
step = pd.Timedelta('5m')
H={}
for name in home.houseBehaviour.keys():
    H[name]=[]

date_sim = h.start_date
while date_sim <= h.end_date:
    if date_sim.minute % 10 == 0:
        home.simulate_step(date_sim)
        for name,app in home.Consumption.items():
            P[name].append(app)
        for name,act in home.houseBehaviour.items():
            H[name].append(act) 

    date_sim=date_sim+step

for name,val in P.items():
	plt.figure(name)
	plt.plot(val,label=name)



for name,val in H.items():
	plt.figure('act_'+name)
	plt.plot(val,label=name)

plt.show()
