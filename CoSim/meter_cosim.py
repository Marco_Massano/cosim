import collections
import pprint
import pickle
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from pathlib import Path
import json
import matplotlib.dates as mdates



class SMModel:

    def __init__(self, name):
        self.name = name

        self.Pnet = 0
        self.Pload = 0
        self.Pprod = 0
        self.Enet = 0
        self.Eload = 0
        self.Eprod = 0
        self.Autocon = 0
        self.Autosuff = 0
        self.Immessa = 0
        self.data = collections.defaultdict(dict)
        self.Ppeak = 0
       # self.measures = collections.defaultdict(dict)

       # self.profiles = collections.defaultdict(dict)

    
    def step(self, collect_data = False, duration = None, num_sec = None, time=0):
        self.duration = duration
        self.num_sec = num_sec
       # self.from_sec = from_sec
        #self.to_sec = to_sec

        Pload = self.Pload
        Pprod = self.Pprod
        self.Pnet = Pload - Pprod 

        Ppeak_temp  = Pload   
        if Ppeak_temp > self.Ppeak:
            self.Ppeak = Ppeak_temp
       

        self.Eload = self.Eload + Pload/6
        self.Eprod = self.Eprod + Pprod/6
        self.Enet = self.Eload - self.Eprod

        if self.Pprod >=  self.Pload:
            self.Autocon = self.Autocon + self.Pload/6
            self.Immessa = self.Immessa  + (self.Pprod - self.Pload)/6
        else:
            self.Autocon = self.Autocon + self.Pprod/6
        self.Autosuff = self.Autocon/self.Eload * 100


        self.collect_data = collect_data 
        if collect_data:


            self.data['Pload'][str(time)] = int(Pload)
            self.data['Pprod'][str(time)] = int(Pprod)
            self.data['Pnet'][str(time)] = int(self.Pnet)


 

class MeterSimulator(object):

    def __init__(self):

        self.models = {}
       # self.data = []



    def add_model(self, name):     

        model = SMModel(name)
        self.models[name] = model
        #self.data.append([])  # Add list for simulation data



    def finalize(self):
        measures = {}
        for eid in self.models.keys():
            model = self.models[eid]
            measures[eid] = {'Eprod':model.Eprod,'Eload':model.Eload,'Autocon':model.Autocon,'Immessa':model.Immessa,'Autosuff':model.Autosuff,'Enet':model.Enet, 'LoadFactor': model.Eload/(model.Ppeak*model.duration*24)}
       


        #Path("./OutputSimResults/%d_days_%d_sez"% (self.models[eid].duration, self.models[eid].num_sec)).mkdir(parents=True, exist_ok=True)
        #with open(f'./OutputSimResults/%d_days_%d_sez/output_values.json' % (self.models[eid].duration, self.models[eid].num_sec), 'w') as outfile:
        #    json.dump(measures, outfile) 

        Path("./OutputSimResults/%d_days_%s_sez"% (self.models[eid].duration, self.models[eid].num_sec)).mkdir(parents=True, exist_ok=True)
        with open(f'./OutputSimResults/%d_days_%s_sez/output_values.json' % (self.models[eid].duration, self.models[eid].num_sec), 'w') as outfile:
            json.dump(measures, outfile) 



        if self.models[eid].collect_data:
            profiles = {}
            for name in self.models.keys():
                model = self.models[name]

                profiles[name] = {'Pload':model.data['Pload'],'Pprod':model.data['Pprod'],'Pnet':model.data['Pnet']}
                #profiles[name] = model.data

            #self.profiles[self.name].append(self.data)

#            Path("./OutputSimResults/%d_days_%d_sez"% (self.models[eid].duration, self.models[eid].num_sec)).mkdir(parents=True, exist_ok=True)
#            with open(f'./OutputSimResults/%d_days_%d_sez/output_profiles.json' % (self.models[eid].duration, self.models[eid].num_sec), 'w') as outfile:
#                json.dump(profiles, outfile)  

            Path("./OutputSimResults/%d_days_%s_sez"% (self.models[eid].duration, self.models[eid].num_sec)).mkdir(parents=True, exist_ok=True)
            with open(f'./OutputSimResults/%d_days_%s_sez/output_profiles.json' % (self.models[eid].duration, self.models[eid].num_sec), 'w') as outfile:
                json.dump(profiles, outfile)  


            sez_profile = pd.read_json('./OutputSimResults/%d_days_%s_sez/output_profiles.json' % (self.models[eid].duration, self.models[eid].num_sec))

            fig, ax = plt.subplots(figsize = (18.5, 10.5))
            ax.set(xlabel = 'Date',
                   ylabel = 'Power [W]',
                   title = 'Sez_Cens: %s' % list(sez_profile.keys())[0])

            date_time = pd.DatetimeIndex(sez_profile[list(sez_profile.keys())[0]]['Pload'].keys())
            
            #fig.autofmt_xdate()
            locator = mdates.AutoDateLocator(minticks=5)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)


            ax.plot(date_time, sez_profile[list(sez_profile.keys())[0]]['Pload'].values())
            ax.plot(date_time, sez_profile[list(sez_profile.keys())[0]]['Pprod'].values())

            plt.gca().legend(('Load','Prod'))
         #   plt.show()

            fig.savefig('OutputSimResults/%d_days_%s_sez/Sim_%d_days_%s_sez' % (self.models[eid].duration, self.models[eid].num_sec, self.models[eid].duration, list(sez_profile.keys())[0]))
            plt.close(fig)  
         

            
