import geopandas as gpd
import os
import pandas as pd
import numpy as np
import pickle
import itertools
import time
from pathlib import Path
import json
from homesimu import Home_new
import collections
path = os.path.dirname('.')
from functools import partial
import matplotlib.pyplot as plt
%matplotlib qt


days = int(input('Hows many days? '))

START_DATE = '2015-09-22 00:00:00' #YYYY/MM/dd 
start_date = pd.to_datetime(START_DATE).tz_localize('UTC')
end_date = start_date + days*pd.Timedelta('1D') - pd.Timedelta('5m')



houses_tot = pd.read_csv('pop_sim/synthetic_households.csv')
houses_tot = houses_tot.set_index(houses_tot['household_id'])
houses_tot = houses_tot.sort_values(by = ['SEZ2011'])

fname = 'pop_sim/persons.pkl'
with open(fname, 'rb') as f:
    families_tot = pickle.load(f) 


houses = houses_tot[0:10]
families  = {key: families_tot[key] for key in houses.index.astype(str)}


model_home = Home.Simulator()

for house in houses.household_id.values:
    model_home.add_model(init_val = families[str(house)], houseid = str(house))

Pload = {}
Plights = {}
Ptv = {}
Pcook = {}
Pleav = {}
Pwash = {}
Pfridge = {}
Piron = {}
Pradio = {}


step = pd.Timedelta('5m')


for i in model_home.models:
    Pload[int(i.houseid)] = []
    Plights[int(i.houseid)] = []
    Ptv[int(i.houseid)] = []
    Pcook[int(i.houseid)] = []
    Pleav[int(i.houseid)] = []
    Pwash[int(i.houseid)] = []
    Pfridge[int(i.houseid)] = []
    Piron[int(i.houseid)] = []
    Pradio[int(i.houseid)] = []


    date_sim = start_date

    while date_sim <= end_date:
        if date_sim.minute % 10 == 0:

            model_home.step(time=date_sim, meter_home=i)
 
            Pload[int(i.houseid)].append(i.Consumption['Aggregate'])
            Plights[int(i.houseid)].append(i.Consumption['Lights'])
            Ptv[int(i.houseid)].append(i.Consumption['TV'])
            Pcook[int(i.houseid)].append(i.Consumption['Cooking'])
            Pleav[int(i.houseid)].append(i.Consumption['Leaving'])
            Pwash[int(i.houseid)].append(i.Consumption['Washingmachine'])
            Pfridge[int(i.houseid)].append(i.Consumption['Fridge'])
            Piron[int(i.houseid)].append(i.Consumption['Iron'])
            Pradio[int(i.houseid)].append(i.Consumption['Radio'])
        date_sim += step

fig_aggr = plt.figure()
plt.title('Loads')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pload:
    plt.plot(Pload[i])

plt.gca().legend(houses.household_id.values)

fig_lights = plt.figure()
plt.title('Lights')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Plights:
    plt.plot(Plights[i])
plt.gca().legend(houses.household_id.values)


fig_tv = plt.figure()
plt.title('TV')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Ptv:
    plt.plot(Ptv[i])
plt.gca().legend(houses.household_id.values)

fig_cook = plt.figure()
plt.title('Cooking')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pcook:
    plt.plot(Pcook[i])
plt.gca().legend(houses.household_id.values)

fig_leav = plt.figure()
plt.title('Leaving')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pleav:
    plt.plot(Pleav[i])
plt.gca().legend(houses.household_id.values)

fig_wash = plt.figure()
plt.title('Washingmachine')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pwash:
    plt.plot(Pwash[i])
plt.gca().legend(houses.household_id.values)

fig_fridge = plt.figure()
plt.title('Fridge')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pfridge:
    plt.plot(Pfridge[i])
plt.gca().legend(houses.household_id.values)

fig_iron = plt.figure()
plt.title('Iron')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Piron:
    plt.plot(Piron[i])
plt.gca().legend(houses.household_id.values)

fig_radio = plt.figure()
plt.title('Radio')
plt.xlabel('Time (10-mins)')
plt.ylabel('Power [W]')
for i in Pradio:
    plt.plot(Pradio[i])
plt.gca().legend(houses.household_id.values)

